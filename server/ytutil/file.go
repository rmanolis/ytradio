package ytutil

import (
	"fmt"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
)

const MAX_MEMORY = 6 * 1024 * 1024

func Files(r *http.Request) ([][]byte, error) {
	form, err := MultiForm(r)
	if err != nil {
		return nil, err
	}
	return ReadFiles(form)
}

func MultiForm(r *http.Request) (*multipart.Form, error) {
	err := r.ParseMultipartForm(MAX_MEMORY)
	return r.MultipartForm, err
}

func ReadFiles(form *multipart.Form) ([][]byte, error) {
	files := form.File
	buf_ls := [][]byte{}
	for _, fileHeaders := range files {
		for _, fileHeader := range fileHeaders {
			log.Println(fileHeader.Filename)
			file, err := fileHeader.Open()
			if err != nil {
				return nil, err
			}
			buf, err := ioutil.ReadAll(file)
			if err != nil {
				return nil, err
			}
			buf_ls = append(buf_ls, buf)
		}
	}
	return buf_ls, nil
}

func DeleteFile(ftype, name string) error {
	path := fmt.Sprintf("static/files/%s/%s", ftype, name)
	err := os.Remove(path)
	if err != nil {
		log.Println(err.Error())
	}
	return err
}
