package ytutil

import (
	"errors"
	"github.com/goincremental/negroni-sessions"
	"net/http"
)

func GetSessionId(r *http.Request) (string, error) {
	session := sessions.GetSession(r)
	id := session.Get("session_id")
	if id == nil {
		return "", errors.New("No session id")
	} else {
		return id.(string), nil
	}
}

func SetSessionId(r *http.Request, session string) {
	ses := sessions.GetSession(r)
	ses.Set("session_id", session)
}
