package ytwspool

import (
	"encoding/json"
	"log"
	"ytradio/server/ytws/ytwsstructs"
)

// hub maintains the set of active connections and broadcasts messages to the
// connections.
type HubPool struct {
	// Registered connections.
	pool *SessionPool

	// Inbound messages from the connections.
	broadcast chan []byte

	onchannel chan map[*ytwsstructs.WSConn][]byte

	// Register requests from the connections.
	register chan *ytwsstructs.WSConn

	// Unregister requests from connections.
	unregister chan *ytwsstructs.WSConn
}

func NewHub() *HubPool {
	h := HubPool{
		broadcast:  make(chan []byte),
		onchannel:  make(chan map[*ytwsstructs.WSConn][]byte),
		register:   make(chan *ytwsstructs.WSConn),
		unregister: make(chan *ytwsstructs.WSConn),
		pool:       NewSessionPool(),
	}
	return &h
}

var Hub = NewHub()
var routes = map[string]func(*ytwsstructs.WSConn, *ytwsstructs.WSMessage){}

func HubRun(rs map[string]func(*ytwsstructs.WSConn, *ytwsstructs.WSMessage)) {
	routes = rs
	Hub.Run()
}

func (h *HubPool) Run() {
	for {
		select {
		case c := <-h.register:
			Hub.pool.Add(c)

		case c := <-h.unregister:
			h.pool.Remove(c)
			close(c.Send)

		case m := <-h.broadcast:
			h.pool.Broadcast(m)

		case m := <-h.onchannel:
			for k, v := range m {
				go routeChannels(k, v)
			}
		}
	}
}

func routeChannels(con *ytwsstructs.WSConn, m []byte) {
	wsm := new(ytwsstructs.WSMessage)
	err := json.Unmarshal(m, wsm)
	if err != nil {
		log.Println("could not unmarshal the wsmessage ", err)
	}
	f, ok := routes[wsm.Channel]
	if ok {
		log.Println(wsm)
		f(con, wsm)
	} else {
		log.Println("Did not find the channel ", wsm.Channel)
	}
}

func Broadcast(m []byte) {
	Hub.broadcast <- m
}

func Register(c *ytwsstructs.WSConn) {
	Hub.register <- c
	wsm := ytwsstructs.WSMessage{
		Channel: "register",
		Message: c.Id,
	}
	f, ok := routes[wsm.Channel]
	if ok {
		f(c, &wsm)
	}

}

func Unregister(c *ytwsstructs.WSConn) {
	log.Println("unregister called for ", c.Id)
	wsm := ytwsstructs.WSMessage{
		Channel: "unregister",
		Message: c.Id,
	}
	f, ok := routes[wsm.Channel]
	if ok {
		f(c, &wsm)
	}

	Hub.unregister <- c
}

func OnChannel(c *ytwsstructs.WSConn, m []byte) {
	v := map[*ytwsstructs.WSConn][]byte{
		c: m,
	}
	Hub.onchannel <- v
}

func ToSession(channel, session string, m []byte) {
	wc, ok := Hub.pool.connections[session]
	if !ok {
		log.Println("pool does not have session ", session)
		return
	}
	wc.ToChannel(channel, m)
}
