package ytwspool

import (
	"sync"
	"ytradio/server/ytutil"
	"ytradio/server/ytws/ytwsstructs"
)

// SessionPool is a structure for thread-safely storing sessions and broadcasting messages to them.
type SessionPool struct {
	mu          sync.RWMutex
	connections map[string]*ytwsstructs.WSConn
}

func NewSessionPool() (p *SessionPool) {
	p = new(SessionPool)
	p.connections = make(map[string]*ytwsstructs.WSConn)
	return p
}

func createSessionId(p *SessionPool) string {
	for {
		id := ytutil.RandSeq(13)
		_, ok := p.connections[id]
		if !ok {
			return id
		}
	}
}

// Add adds the given session to the session pool.
func (p *SessionPool) Add(c *ytwsstructs.WSConn) {
	p.mu.Lock()
	defer p.mu.Unlock()
	c.Id = createSessionId(p)
	p.connections[c.Id] = c
}

// Remove removes the given session from the session pool.
// It is safe to remove non-existing sessions.
func (p *SessionPool) Remove(c *ytwsstructs.WSConn) {
	p.mu.Lock()
	defer p.mu.Unlock()
	_, ok := p.connections[c.Id]
	if ok {
		delete(p.connections, c.Id)
	}
}

// Broadcast sends the given message to every session in the pool.
func (p *SessionPool) Broadcast(m []byte) {
	p.mu.RLock()
	defer p.mu.RUnlock()
	for _, c := range p.connections {
		select {
		case c.Send <- m:
		default:
			p.Remove(c)
			close(c.Send)
		}
	}
}
