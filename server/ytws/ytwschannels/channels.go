package ytwschannels

import (
	"encoding/json"
	"log"
	"ytradio/server/ytglobals"
	"ytradio/server/ytmodels"
	"ytradio/server/ytws/ytwspool"
	"ytradio/server/ytws/ytwsstructs"
)

var listener = Listener{}
var admin = Admin{}
var chat = Chat{}

var Routes = map[string]func(*ytwsstructs.WSConn, *ytwsstructs.WSMessage){
	"broadcast":     onBroadcast,
	"register":      onRegister,
	"unregister":    onUnregister,
	"sendMySession": onSendMySession,
	//for admin
	"adminCreateRadio":            admin.createRadio,
	"adminPlayVideo":              admin.playVideo,
	"adminCurrentVideoToListener": admin.currentVideoToListener,
	"adminCurrentVideo":           admin.currentVideo,
	"adminStopVideo":              admin.stopVideo,
	"adminPauseVideo":             admin.pauseVideo,

	"adminShowImage": admin.showImage,

	"adminPlaySound":  admin.playSound,
	"adminStopSound":  admin.stopSound,
	"adminPauseSound": admin.pauseSound,

	"adminPlayRecord": admin.playRecord,

	//for listener
	"listenerConnectRadio": listener.connectRadio,

	//for chat
	"setUsername":  chat.SetUsername,
	"sendMessage":  chat.SendMessage,
	"chatUsers":    chat.Users,
	"proposeVideo": chat.ProposeVideo,
}

func onRegister(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {

}

func onUnregister(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	dt := ytglobals.GetDT()
	defer dt.Close()
	db := dt.DB(ytglobals.DBName())
	log.Println("closed ", con.Id)

	tr, err := ytmodels.GetTempRadio(db, con.Id)
	if err == nil {
		for _, v := range tr.Listeners {
			ytwspool.ToSession("radioClosed", v, []byte{})
		}

		tr.Delete(db)
		return
	}

	tr, err = ytmodels.FindRadioByListener(db, con.Id)
	if err == nil {
		tr.RemoveListener(con.Id)
		tr.Update(db)
		ytwspool.ToSession("listenerLeft", tr.Name, []byte(con.Id))
		return
	}
	log.Println(err.Error())
}
func onBroadcast(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	out, _ := json.Marshal(wsm)
	ytwspool.Broadcast(out)
}

func onSendMySession(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("sending my session")
	con.ToChannel("mySession", []byte(con.Id))
}
