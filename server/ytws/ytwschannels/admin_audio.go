package ytwschannels

import (
	"encoding/json"
	"log"
	"ytradio/server/ytws/ytwspool"
	"ytradio/server/ytws/ytwsstructs"
)

type SoundMsg struct {
	SoundId  string
	Listener string
	Time     float64
}

func (a Admin) playSound(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("play sound ", wsm.Message)
	bmsg := []byte(wsm.Message)
	im := new(SoundMsg)
	err := json.Unmarshal(bmsg, &im)
	if err != nil {
		log.Println(err.Error())
		return
	}

	if im.Listener == "" {
		sentToAll(con, im, "playSound")
	} else {
		ytwspool.ToSession("playSound", im.Listener, bmsg)
	}
}

func (a Admin) stopSound(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("stop sound ", wsm.Message)
	bmsg := []byte(wsm.Message)
	im := new(SoundMsg)
	err := json.Unmarshal(bmsg, &im)
	if err != nil {
		log.Println(err.Error())
		return
	}
	sentToAll(con, im, "stopSound")
}

func (a Admin) pauseSound(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("pause sound ", wsm.Message)
	bmsg := []byte(wsm.Message)
	im := new(SoundMsg)
	err := json.Unmarshal(bmsg, &im)
	if err != nil {
		log.Println(err.Error())
		return
	}
	sentToAll(con, im, "pauseSound")
}

func (a Admin) playRecord(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("play record ", wsm.Message)
	bmsg := []byte(wsm.Message)
	im := new(SoundMsg)
	err := json.Unmarshal(bmsg, &im)
	if err != nil {
		log.Println(err.Error())
		return
	}
	sentToAll(con, im, "playRecord")

}
