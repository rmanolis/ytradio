package ytwschannels

import (
	"encoding/json"
	"log"
	"ytradio/server/ytglobals"
	"ytradio/server/ytmodels"
	"ytradio/server/ytws/ytwspool"
	"ytradio/server/ytws/ytwsstructs"
)

type Chat struct{}

func (c Chat) SetUsername(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	dt := ytglobals.GetDT()
	defer dt.Close()
	db := dt.DB(ytglobals.DBName())
	username := wsm.Message
	log.Println(username)
	tr, _ := ytmodels.FindRadio(db, con.Id)

	chu, err := ytmodels.FindChatUser(db, tr.Id, con.Id)
	if err == nil {
		return
	}

	chu, err = ytmodels.NewChatUser(db, tr.Id, con.Id, username)
	if err != nil {
		log.Println(err.Error())
	}
	out, _ := json.Marshal(chu)
	ytwspool.ToSession("newChatUser", tr.Name, out)
	for _, v := range tr.Listeners {
		ytwspool.ToSession("newChatUser", v, out)
	}
}

func (c Chat) SendMessage(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	dt := ytglobals.GetDT()
	defer dt.Close()
	db := dt.DB(ytglobals.DBName())
	text := wsm.Message
	log.Println(text)

	tr, _ := ytmodels.FindRadio(db, con.Id)
	cu, _ := ytmodels.FindChatUser(db, tr.Id, con.Id)
	msg, _ := cu.NewMessage(db, text)
	out, _ := json.Marshal(msg)
	ytwspool.ToSession("newMessage", tr.Name, out)
	for _, v := range tr.Listeners {
		ytwspool.ToSession("newMessage", v, out)
	}
}

func (c Chat) Users(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	dt := ytglobals.GetDT()
	defer dt.Close()
	db := dt.DB(ytglobals.DBName())
	tr, err := ytmodels.FindRadio(db, con.Id)
	if err != nil {
		log.Println(err.Error())
	}
	chuls, err := tr.ChatUsers(db)
	if err != nil {
		log.Println(err.Error())
	}

	log.Println(chuls)
	out, _ := json.Marshal(chuls)
	ytwspool.ToSession("chatUsers", con.Id, out)
}

type VideoJSON struct {
	VideoId  string
	Username string
}

func (c Chat) ProposeVideo(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	dt := ytglobals.GetDT()
	defer dt.Close()
	db := dt.DB(ytglobals.DBName())
	tr, _ := ytmodels.FindRadio(db, con.Id)
	cu, _ := ytmodels.FindChatUser(db, tr.Id, con.Id)

	videoId := wsm.Message
	vj := VideoJSON{videoId, cu.Name}
	out, _ := json.Marshal(vj)
	ytwspool.ToSession("proposedVideo", tr.Name, out)
}
