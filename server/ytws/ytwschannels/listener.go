package ytwschannels

import (
	"log"
	"ytradio/server/ytws/ytwspool"

	"ytradio/server/ytglobals"
	"ytradio/server/ytmodels"
	"ytradio/server/ytws/ytwsstructs"
)

type Listener struct{}

func (l Listener) connectRadio(con *ytwsstructs.WSConn,
	wsm *ytwsstructs.WSMessage) {
	log.Println("listener connects to ", wsm.Message)
	dt := ytglobals.GetDT()
	defer dt.Close()
	db := dt.DB(ytglobals.DBName())
	sradio := wsm.Message
	tr, err := ytmodels.GetTempRadio(db, sradio)
	if err != nil {
		log.Println("listener tries to connect to" +
			" non-existent radio")
		return
	}
	tr.AddListener(con.Id)
	err = tr.Update(db)
	if err != nil {
		log.Println("couldnt load listener")
		return
	}
	ytwspool.ToSession("newListener", sradio, []byte(con.Id))
}
