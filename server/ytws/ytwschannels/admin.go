package ytwschannels

import (
	"encoding/json"
	"log"
	"ytradio/server/ytglobals"
	"ytradio/server/ytmodels"
	"ytradio/server/ytws/ytwspool"
	"ytradio/server/ytws/ytwsstructs"
)

type Admin struct{}

/*
	admin.createRadio -> front listener.radioCreated
	admin.playVideo -> front listener.playVideo
	admin.currentVideo -> front listener.currentVideo
	admin.stopVideo  -> front listener.stopVideo
	admin.pauseVideo -> front listener.pauseVideo
*/
func (a Admin) createRadio(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("create new temporary radio")
	dt := ytglobals.GetDT()
	defer dt.Close()
	db := dt.DB(ytglobals.DBName())
	tr, err := ytmodels.NewTempRadio(db, con.Id)
	res := tr.Secret
	if err != nil {
		res = ""
	}
	con.ToChannel("radioCreated", []byte(res))
}

type VideoMsg struct {
	Listener string
	VideoId  string
	Time     float64
}

func sentToAll(con *ytwsstructs.WSConn, vm interface{},
	channel string) {
	dt := ytglobals.GetDT()
	defer dt.Close()
	db := dt.DB(ytglobals.DBName())
	tr, err := ytmodels.GetTempRadio(db, con.Id)
	if err != nil {
		log.Println("Error: ", err.Error())
		return
	}

	out, err := json.Marshal(vm)
	if err != nil {
		log.Println("Error: ", err.Error())
		return
	}

	for _, v := range tr.Listeners {
		ytwspool.ToSession(channel, v, out)
	}
}

func (a Admin) playVideo(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("play video ", wsm.Message)
	vm := new(VideoMsg)
	vm.VideoId = wsm.Message
	sentToAll(con, vm, "playVideo")
}

func (a Admin) currentVideo(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("play current video ", wsm.Message)
	bmsg := []byte(wsm.Message)
	vm := new(VideoMsg)
	err := json.Unmarshal(bmsg, &vm)
	if err != nil {
		log.Println(err.Error())
		return
	}

	sentToAll(con, vm, "currentVideo")
}

//TODO delete this function and change current video
func (a Admin) currentVideoToListener(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("play current video ", wsm.Message)
	bmsg := []byte(wsm.Message)
	vm := new(VideoMsg)
	err := json.Unmarshal(bmsg, &vm)
	if err != nil {
		log.Println(err.Error())
		return
	}

	ytwspool.ToSession("currentVideo", vm.Listener, bmsg)
}

func (a Admin) stopVideo(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("stop video " + wsm.Message)
	vm := new(VideoMsg)
	vm.VideoId = wsm.Message
	sentToAll(con, vm, "stopVideo")
}

func (a Admin) pauseVideo(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("stop video " + wsm.Message)
	vm := new(VideoMsg)
	vm.VideoId = wsm.Message
	sentToAll(con, vm, "stopVideo")
}
