package ytwschannels

import (
	"encoding/json"
	"log"
	"ytradio/server/ytws/ytwspool"
	"ytradio/server/ytws/ytwsstructs"
)

type ImageMsg struct {
	Image    string
	Listener string
}

func (a Admin) showImage(con *ytwsstructs.WSConn, wsm *ytwsstructs.WSMessage) {
	log.Println("show image ", wsm.Message)
	bmsg := []byte(wsm.Message)
	im := new(ImageMsg)
	err := json.Unmarshal(bmsg, &im)
	if err != nil {
		log.Println(err.Error())
		return
	}

	if im.Listener == "" {
		sentToAll(con, im, "showImage")
	} else {
		ytwspool.ToSession("showImage", im.Listener, bmsg)
	}
}
