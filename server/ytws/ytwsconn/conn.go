package ytwsconn

import (
	"log"
	"net/http"
	"time"
	"ytradio/server/ytws/ytwspool"
	"ytradio/server/ytws/ytwsstructs"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// readPump pumps messages from the websocket connection to the hub.
func ReadPump(c *ytwsstructs.WSConn) {
	defer func() {
		ytwspool.Unregister(c)
		c.WS.Close()
	}()
	c.WS.SetReadLimit(maxMessageSize)
	c.WS.SetReadDeadline(time.Now().Add(pongWait))
	c.WS.SetPongHandler(func(string) error {
		c.WS.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})

	for {
		_, message, err := c.WS.ReadMessage()
		if err != nil {
			break
		}
		ytwspool.OnChannel(c, message)
	}
}

// write writes a message with the given message type and payload.
func WriteWithPayload(c *ytwsstructs.WSConn, mt int, payload []byte) error {
	c.WS.SetWriteDeadline(time.Now().Add(writeWait))
	return c.WS.WriteMessage(mt, payload)
}

// writePump pumps messages from the hub to the websocket connection.
func WritePump(c *ytwsstructs.WSConn) {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.WS.Close()
	}()
	for {
		select {
		case message, ok := <-c.Send:
			if !ok {
				WriteWithPayload(c, websocket.CloseMessage, []byte{})
				return
			}
			if err := WriteWithPayload(c, websocket.TextMessage, message); err != nil {
				return
			}
		case <-ticker.C:
			if err := WriteWithPayload(c, websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

// serverWs handles websocket requests from the peer.
func ServeWs(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", 405)
		return
	}
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(ws.RemoteAddr())
	c := &ytwsstructs.WSConn{Send: make(chan []byte, 256), WS: ws}
	ytwspool.Register(c)
	go WritePump(c)
	ReadPump(c)
}
