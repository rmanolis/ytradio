package main

import (
	"net/http"
	"ytradio/server/ytctrls/ytcfiles"
	"ytradio/server/ytglobals"
	"ytradio/server/ytutil"
	"ytradio/server/ytws/ytwschannels"
	"ytradio/server/ytws/ytwsconn"
	"ytradio/server/ytws/ytwspool"

	"github.com/codegangsta/negroni"
	"github.com/goincremental/negroni-sessions"
	"github.com/goincremental/negroni-sessions/cookiestore"
	"github.com/gorilla/mux"
)

func Welcome(w http.ResponseWriter, req *http.Request) {
	http.ServeFile(w, req, "static/index.html")
}

func main() {
	go ytwspool.HubRun(ytwschannels.Routes)

	n := negroni.Classic()
	static := negroni.NewStatic(http.Dir("static"))
	static.Prefix = "/static"
	n.Use(static)

	gdt := ytutil.NewDataStore("radio", "127.0.0.1", "27017")
	ytglobals.SetDT(gdt)

	store := cookiestore.New([]byte("aaaalasdlasld"))
	n.Use(sessions.Sessions("global_session_store", store))
	router := mux.NewRouter()
	router.HandleFunc("/", Welcome).Methods("GET")
	ytcfiles.Routes(router)
	router.HandleFunc("/ws", ytwsconn.ServeWs)
	n.Use(negroni.HandlerFunc(gdt.MgoMiddleware))
	n.UseHandler(router)
	n.Run(":8080")
}
