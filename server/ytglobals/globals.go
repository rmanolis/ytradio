package ytglobals

import (
	"net/http"
	"ytradio/server/ytutil"

	"github.com/gorilla/context"
	"gopkg.in/mgo.v2"
)

var gdt = new(ytutil.DataStore)

func GetDT() *mgo.Session {
	return gdt.Session.Clone()
}

func SetDT(dt *ytutil.DataStore) {
	gdt = dt
}

func GetDB(r *http.Request) *mgo.Database {
	db := context.Get(r, gdt.DBName)
	return db.(*mgo.Database)
}

func DBName() string {
	return gdt.DBName
}
