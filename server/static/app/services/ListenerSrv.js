app.factory("ListenerSrv", function($q,WebsocketSrv){
  var obj ={}
  obj.connectRadio = function(radio){
    WebsocketSrv.toChannel("listenerConnectRadio", radio);
  }

  obj.onRadioClosed = function(cb){
    WebsocketSrv.onChannel("radioClosed",function(msg){
        cb(msg);
      }); 
  }

  obj.onPlayVideo = function(cb){
      WebsocketSrv.onChannel("playVideo",function(msg){
        cb(msg);
      }); 
  }

  obj.onStopVideo = function(cb){
      WebsocketSrv.onChannel("stopVideo",function(msg){
        cb(msg);
      }); 
  }

  obj.onPauseVideo = function(cb){
      WebsocketSrv.onChannel("pauseVideo",function(msg){
        cb(msg);
      }); 
  }

  //returns {Listener, VideoId, Time}
  obj.onCurrentVideo = function(cb){
      WebsocketSrv.onChannel("currentVideo",function(msg){
        cb(msg);
      });
  }


  obj.onShowImage = function(cb){
     WebsocketSrv.onChannel("showImage",function(msg){
        cb(msg);
      });
  }

  obj.onPlaySound = function(cb){
     WebsocketSrv.onChannel("playSound",function(msg){
        cb(msg);
      });
  }

  obj.onPauseSound = function(cb){
     WebsocketSrv.onChannel("pauseSound",function(msg){
        cb(msg);
      });
  }

  obj.onStopSound = function(cb){
     WebsocketSrv.onChannel("stopSound",function(msg){
        cb(msg);
      });
  }

  obj.onPlayRecord = function(cb){
     WebsocketSrv.onChannel("playRecord",function(msg){
        cb(msg);
      });
  }



  
 


  return obj
});
