app.factory("ChannelsSrv",function($q,WebsocketSrv){
  var obj = {};
  obj.onMySession = function () {
    WebsocketSrv.toChannel("sendMySession","");
    return $q(function(resolve, reject) {
      WebsocketSrv.onChannel("mySession",function(msg){
        resolve(msg);
      });
    });
  }

  return obj;
})
