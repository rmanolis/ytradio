app.factory("YoutubeSrv",function($timeout,toastr){
  var obj = {};
  obj.openPlayer = function(videoId,time){
    var pl = {
      height: '300',
      width: '200',
      videoId:videoId,
      events: {
        'onReady': onPlayerReady,
        'onError': onError,
        'onStateChange':onPlayerStateChange,  
      }
    }
    if(time){
      pl.startSeconds = time;
    }
    return new YT.Player('player', pl);
  }

  function onPlayerStateChange(event,element){
    console.log('state change ' + event.data);
   if (event.data == 5) {
        //Get rid of the player
        event.target.destroy();
    }
  }
  obj.cbPlayer = function(videoId,cb){
    if(YT.Player){
      var player = new YT.Player('player', {
        height: '300',
        width: '200',
        videoId: videoId,
        events: {
          'onReady': onPlayerReady,
        }
      });
      $timeout(function(){
        cb(player);
      },2000);
    }else{
      toastr.error("Restart");
    }
  }


  function onPlayerReady(event) {
    event.target.playVideo();
  }

  function onError(event){
     toastr.error("Wrong video"); 
  }


  return obj;
})
