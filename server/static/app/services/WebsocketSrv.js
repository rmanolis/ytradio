app.factory("WebsocketSrv",function($websocket,toastr){
  var ws = $websocket(
      //"ws://localhost:8080/ws");
      "ws://188.166.72.228/ws");
  var obj = {};
  obj.onChannel = function(channel,cb){
    ws.onMessage(function(evt) {
      var msg = JSON.parse(evt.data); 
      if(msg.channel == channel){
        cb(msg.message)
      }
    })
  };

  obj.onClose = function(cb){
    ws.onClose(function(evt){
      toastr.error("Connection is closed!");
    })
  }


  obj.toChannel = function(channel,message){
    ws.send(JSON.stringify({channel: channel, 
      message:message }));
  }

  return obj;
})
