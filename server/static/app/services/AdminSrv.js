app.factory("AdminSrv", function($http,WebsocketSrv){
  var obj ={}
  //The msg is boolean
  obj.createRadio = function(cb){
    WebsocketSrv.toChannel("adminCreateRadio","");
    WebsocketSrv.onChannel("radioCreated",function(msg){
      $http.post('/cookie',msg);
      cb(msg);
    });
  }

  obj.playVideo = function(video){
    WebsocketSrv.toChannel("adminPlayVideo",video);
  }

  obj.stopVideo = function(video){
    WebsocketSrv.toChannel("adminStopVideo",video);
  }

  obj.pauseVideo = function(video){
    WebsocketSrv.toChannel("adminPauseVideo",video);
  }

  obj.currentVideo = function(video,time){
    WebsocketSrv.toChannel("adminCurrentVideo", 
        JSON.stringify({
          VideoId: video,
          Time:time,
        }))
  }


  obj.onNewListener = function(cb){ 
    WebsocketSrv.onChannel("newListener",function(msg){
      cb(msg);
    });   
  }

  obj.onListenerLeft = function(cb){
    WebsocketSrv.onChannel("listenerLeft",function(msg){
      cb(msg);
    }); 
  }


  obj.currentVideoToListener = function(listener,video,time){
    WebsocketSrv.toChannel("adminCurrentVideoToListener", JSON.stringify({
      Listener:listener,
      VideoId: video,
      Time:time,
    }))
  }

  //for image
  obj.showImage = function(image, listener){
    WebsocketSrv.toChannel("adminShowImage",JSON.stringify({
      Listener:listener,
      Image: image,
    }))
  }

  // for sound

  obj.playSound = function(sound, time, listener){
    WebsocketSrv.toChannel("adminPlaySound",JSON.stringify({
      Listener:listener,
      SoundId: sound,
      Time: time,
    }))
  }

  obj.stopSound = function(sound){
     WebsocketSrv.toChannel("adminStopSound",JSON.stringify({
      SoundId: sound,
    }))
  }

  obj.pauseSound = function(sound, time){
    WebsocketSrv.toChannel("adminPauseSound",JSON.stringify({
      SoundId: sound,
      Time: time,
    }))
  }

  obj.playRecord = function(sound){
    WebsocketSrv.toChannel("adminPlayRecord",JSON.stringify({
      SoundId: sound,
    }))
  }





  return obj;
});
