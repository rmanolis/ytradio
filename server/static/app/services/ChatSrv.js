app.factory("ChatSrv",["WebsocketSrv",
  function(WebsocketSrv){
    var obj = {};
    
    obj.onNewMessage = function(cb){
      WebsocketSrv.onChannel("newMessage",function(msg){
        cb(msg)
      })
    }

    obj.onNewChatUser = function(cb){
      WebsocketSrv.onChannel("newChatUser",function(msg){
        cb(msg)
      })
    }

    obj.onChatUsers = function(cb){
       WebsocketSrv.onChannel("chatUsers",function(msg){
        cb(msg)
      })
    }

    obj.onEditChatUser = function(cb){
      WebsocketSrv.onChannel("editChatUser",function(msg){
        cb(msg)
      })

    }

    obj.onProposedVideo = function(cb){
      WebsocketSrv.onChannel("proposedVideo",function(msg){
        cb(msg)
      })
    }


    obj.setUsername = function(username){
      WebsocketSrv.toChannel("setUsername",username)
    }
    
    obj.sendMessage = function(message){
      WebsocketSrv.toChannel("sendMessage",message)
    }

    obj.chatUsers = function(){
      WebsocketSrv.toChannel("chatUsers","");
    }

    obj.proposeVideo = function(videoId){
      WebsocketSrv.toChannel("proposeVideo",videoId)
    }




    return obj;
  }
]);
