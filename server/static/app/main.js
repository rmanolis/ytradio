//loading Lodash library in angular
var lodash = angular.module('lodash', []);
lodash.factory('_', function () {
  return window._; // assumes underscore has already been loaded on the page
});


var app = angular.module('App', ['ngRoute', 
    'ui.bootstrap','ui.bootstrap.progressbar','luegg.directives',
    ,'toastr','ngWebSocket','ngFileUpload','ngAudio']);


app.run(function($rootScope,$http,ChannelsSrv){
  
});


app.config(function ($routeProvider) {

  $routeProvider
    .when('/', {
      templateUrl: '/static/app/pages/main.html',
      controller: 'MainCtrl'
    })
  
 
 
  .when('/listener/:id',{
    templateUrl: '/static/app/pages/listener/listener.html',
    controller: 'ListenerCtrl'
  })

});

