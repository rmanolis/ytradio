app.controller("ListenerCtrl",
    ["$scope","$routeParams","YoutubeSrv","ChannelsSrv",
    "ListenerSrv","toastr","ngAudio",
    function($scope,$routeParams,YoutubeSrv,
      ChannelsSrv,ListenerSrv,toastr,ngAudio){
 
    ListenerSrv.connectRadio($routeParams.id);      
    ListenerSrv.onCurrentVideo(function(data){
      var vm = JSON.parse(data);
      console.log("play current "+ vm.VideoId); 
      $scope.player = YoutubeSrv.openPlayer(vm.VideoId);
      $scope.player.stopVideo()
      $scope.player.cueVideoById(vm.VideoId,vm.Time);
    });

  


  ChannelsSrv.onMySession().then(function(session){
    console.log("listener "+session);
    $scope.my_session = session;
  })

  ListenerSrv.onRadioClosed(function(){
    toastr.warning("The current radio closed");
  })
  ListenerSrv.onPlayVideo(function(data){
    var vm = JSON.parse(data);
    console.log("play "+vm.VideoId); 
    $scope.player.loadVideoById(vm.VideoId)
  })

  ListenerSrv.onStopVideo(function(data){
    var vm = JSON.parse(data);
    console.log("stop "+vm.VideoId); 
    $scope.player.stopVideo()
  })

  ListenerSrv.onPauseVideo(function(data){
    var vm = JSON.parse(data);
    console.log("pause "+vm.VideoId); 
    $scope.player.pauseVideo()
  })

  $scope.imagesrc="";
  ListenerSrv.onShowImage(function(data){
    var im = JSON.parse(data);
    console.log(im);
    $scope.imagesrc = im.Image;
  });


  $scope.playerSound = null;
  ListenerSrv.onPlaySound(function(data){
    var sm = JSON.parse(data);
    console.log(sm);
    if(sm.Time > 0){
      $scope.playerSound.currentTime = sm.Time;
    }else{
      $scope.playerSound = ngAudio.load("static/files/sound/"+sm.SoundId);
    }

    $scope.playerSound.play();
  })

  ListenerSrv.onStopSound(function(data){
    var sm = JSON.parse(data);
    console.log(sm);
      $scope.playerSound.stop();
      $scope.playerSound=null;
    
  });

  ListenerSrv.onPauseSound(function(data){
    var sm = JSON.parse(data);
    console.log(sm);
    $scope.playerSound.pause();
  });

  ListenerSrv.onPlayRecord(function(data){
    var sm = JSON.parse(data);
    console.log(sm);
    ngAudio.play("static/files/record/"+sm.SoundId);
  });
  

}]);
