app.controller("MainCtrl",
    ["$scope","$location","AdminSrv","ChannelsSrv","AdminSrv","toastr",
    function($scope, $location, AdminSrv,
      ChannelsSrv, AdminSrv,toastr){
 $scope.isAdmin = false;
 $scope.openRadio = function(){
  $scope.isAdmin = true;
  AdminSrv.createRadio(function(msg){
    console.log(msg);
  })
 }

  $scope.countListeners = 0;
  ChannelsSrv.onMySession().then(function(session){
    console.log(session);
    $scope.my_session = session;
  })
  AdminSrv.onNewListener(function(listener){
    toastr.info("New listener");
     
    $scope.countListeners += 1;
  });
  AdminSrv.onListenerLeft(function(listener){
    toastr.warning("A listener left");
    $scope.countListeners -= 1;

  });

}])
