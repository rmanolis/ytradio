app.controller("ChatCtrl",[
    "$scope","ChatSrv","toastr","HelperSrv",
    function($scope, ChatSrv,toastr,HelperSrv){
      $scope.myName = "";
      $scope.users = [];
      $scope.message=" ";
      $scope.propose = {
        url:""
      }

      $scope.setName = function(name){
        if(name.length > 3){
          angular.forEach($scope.users,function(user){
            if(user.Name === name){
              toastr.error("The name already taken");
              return
            }
          })
          ChatSrv.setUsername(name);
          $scope.myName = angular.copy(name);
          $scope.name = "";
        }else{
          toastr.error("Add more characters to the name");
        }
      }
      ChatSrv.chatUsers();

      
      ChatSrv.onNewChatUser(function(data){
        var user = JSON.parse(data);
        $scope.users.push(user);
        
      })

      ChatSrv.onChatUsers(function(data){
        var users = JSON.parse(data);
        $scope.users = users;
      })

      
      $scope.messages = [];

      ChatSrv.onNewMessage(function(data){
        var msg = JSON.parse(data);
        console.log(msg);
        $scope.messages.push(msg);
      });

      $scope.messenger = {
        message:""
      };
      $scope.messenger.sendMessage = function(){
       ChatSrv.sendMessage($scope.messenger.message);
       $scope.messenger.message="";
      }


      $scope.proposeVideo = function(url){
        var id = HelperSrv.getURLParameters(url,"v");
        ChatSrv.proposeVideo(id);
        $scope.propose.url = "";
      }
      
}]);
