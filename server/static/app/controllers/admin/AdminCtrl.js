app.controller("AdminCtrl",
    ["$scope","$http","ChannelsSrv","YoutubeSrv","AdminSrv","toastr","ChatSrv","HelperSrv",
    function($scope,$http, ChannelsSrv, YoutubeSrv,AdminSrv, toastr,ChatSrv,HelperSrv ){

  $scope.video = "";
  ChannelsSrv.onMySession().then(function(session){
    console.log(session);
    $scope.my_session = session;
  })
  AdminSrv.onNewListener(function(listener){
    var time = $scope.player.getCurrentTime();
    AdminSrv.currentVideoToListener(listener, $scope.video,time); 
  });

  $scope.proposedVideos = []; 

  ChatSrv.onProposedVideo(function(data){
    var video = JSON.parse(data);
    $scope.proposedVideos.push(video);
  })


  $scope.pauseRadio = function(){
    $scope.player.pauseVideo();
    AdminSrv.pauseVideo($scope.video);
  }

  $scope.playRadio = function(){
    $scope.player.playVideo();
    var time = $scope.player.getCurrentTime();    
    AdminSrv.currentVideo($scope.video, time);
  }

  $scope.stopRadio = function(){
    $scope.player.stopVideo();
    AdminSrv.stopVideo($scope.video);
    $scope.video="";
  }

  $scope.syncRadio = function(){
    var time = $scope.player.getCurrentTime();
    AdminSrv.currentVideo($scope.video, time);
  }

  $scope.playlist = [];
  $scope.url = "";

  $scope.add = function(url){
    var id = HelperSrv.getURLParameters(url,"v");
    if(id){
      $scope.playlist.push(id);
      $scope.url = "";
    }

  }

  $scope.remove = function(url){
    _.remove($scope.playlist,function(v){
      return url == v;
    });
  }

  $scope.removeProposed = function(id){
    _.remove($scope.proposedVideos,function(v){
      return id == v;
    });
  }


  $scope.play = function(id){
    $scope.player = YoutubeSrv.openPlayer(id); 
    AdminSrv.playVideo(id);
    $scope.video=id;
  }

}])
