app.controller("AdminFileCtrl",
    ["$scope","$http","Upload","ngAudio","toastr","FileSrv","AdminSrv",
    function($scope, $http,Upload,ngAudio,
      toastr, FileSrv, AdminSrv){
  $scope.fileByType = {
    image:null,
    sound:null,
    record:null,
  }
  $scope.listFiles = {
    image:[],
    sound:[],
    record:[],
  }
  $scope.progressFile = {
    image:0,
    sound:0,
    record:0,
  }
  AdminSrv.onNewListener(function(listener){
    AdminSrv.showImage($scope.imagesrc,listener);
  });


  function loadFiles(ftype){
    FileSrv.listFiles(ftype).success(function(data){
      console.log(data);
      $scope.listFiles[ftype] = data;
    })
  }

  loadFiles("image");
  loadFiles("sound");
  loadFiles("record");


  $scope.upload = function(ftype, files){
    $scope.fileByType[ftype]=files;
  }
  $scope.uploadFile = function (ftype,cb) {
    var files = $scope.fileByType[ftype];
    for (var i = 0; i < files.length; i++) {
      var file = files[i];
      Upload.upload({
        url: '/files/'+ftype,
        fields: { Name: file.name},
        file: file
      }).progress(function (evt) {
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        $scope.progressFile[ftype] = progressPercentage;
        console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
      }).success(function (data, status, headers, config) {
        console.log(data);
        loadFiles(ftype);
        toastr.info("File have been upload");
        $scope.fileByType[ftype] = [];
        $scope.progressFile[ftype] = 0;
        if(cb){
          cb(data);
        }
      }).error(function(data){
        toastr.error(data);
         $scope.progressFile[ftype] = 0;
        $scope.fileByType[ftype] = [];        
      });
    }  
  }; 

  $scope.soundPlayer = null;
  $scope.soundId = "";
  $scope.soundName ="";
  $scope.loadSound = function(id,name){
    if(!_.isNull($scope.soundPlayer)){
      $scope.soundPlayer.stop();
      $scope.soundPlayer = null;
    }
    $scope.soundId = id;
    $scope.soundName = name;
    $scope.soundPlayer = ngAudio.load("static/files/sound/"+id );
  }
  
  $scope.isMute = false;
  $scope.playSound = function(){
    $scope.soundPlayer.play();
    var time = $scope.soundPlayer.currentTime;
    AdminSrv.playSound($scope.soundId,time,"");
    $scope.isMute = false;
  }

  $scope.stopSound = function(){
    $scope.soundPlayer.stop();
    $scope.soundPlayer = null;
    $scope.soundId = "";
    $scope.soundName = "";
    AdminSrv.stopSound($scope.soundId);
  }

  $scope.pauseSound = function(){
    $scope.soundPlayer.pause();
    var time = $scope.soundPlayer.currentTime;
    AdminSrv.pauseSound($scope.soundId,time);
  }

  $scope.muteSound = function(){
    $scope.soundPlayer.muting = !$scope.soundPlayer.muting;
    $scope.isMute= $scope.soundPlayer.muting;
  }

 

  $scope.imagesrc = "";

  $scope.showImage = function(id){
    $scope.imagesrc = "static/files/image/"+id ;
    AdminSrv.showImage($scope.imagesrc,"");
  }

  $scope.removeImage = function(){
    $scope.imagesrc = "";
    AdminSrv.showImage($scope.imagesrc,"");
  }

  


  var navigator = window.navigator;
  navigator.getUserMedia = (
      navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia ||
      navigator.msGetUserMedia
      );

  var Context = window.AudioContext || window.webkitAudioContext;
  var context = new Context();

  var mediaStream;
  var rec;
  $scope.isRecording = false;
  function record () {
    // ask for permission and start recording
    navigator.getUserMedia({audio: true}, function(localMediaStream){
      mediaStream = localMediaStream;

      // create a stream source to pass to Recorder.js
      var mediaStreamSource = context.createMediaStreamSource(localMediaStream);

      // create new instance of Recorder.js using the mediaStreamSource
      rec = new Recorder(mediaStreamSource, {
        // pass the path to recorderWorker.js file here
        workerPath: '/static/lib/record/recorderWorker.js'
      });
      $scope.$apply(function () {
        $scope.isRecording = true;
      })
      
      // start recording
      rec.record();
    }, function(err){
      toastr.error('Browser not supported');
      console.log(err);
    });
  }

  function stopRecord(){
    $scope.isRecording = false;

    // stop the media stream
    mediaStream.stop();

    // stop Recorder.js
    rec.stop();
    
    // export it to WAV
    rec.exportWAV(function(e){
      console.log(e);
      rec.clear();

      e.lastModifiedDate = new Date();
      e.name = e.lastModifiedDate.toString();
      $scope.upload("record", [e]);
      $scope.uploadFile("record",
         function(data){
            AdminSrv.playRecord(data.FileId);
          });
    });
  }

  $scope.record =record;
  $scope.stopRecord = stopRecord;

}])
