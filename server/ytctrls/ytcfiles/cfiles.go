package ytcfiles

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"log"
	"net/http"
	"ytradio/server/ytglobals"
	"ytradio/server/ytmodels"
	"ytradio/server/ytutil"

	"github.com/gorilla/mux"
	"github.com/rakyll/magicmime"
	"gopkg.in/mgo.v2/bson"
)

type FileBody struct {
	Name   string
	FileId string
}

func AddCookie(w http.ResponseWriter, r *http.Request) {
	session, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	ytutil.SetSessionId(r, string(session))
	w.WriteHeader(http.StatusAccepted)
}

func AddFile(w http.ResponseWriter, r *http.Request) {
	db := ytutil.GetDB(r, ytglobals.DBName())
	vars := mux.Vars(r)
	ftname := vars["type"]
	fto := ytmodels.NewFileType(ftname)
	if fto == nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	session_id, err := ytutil.GetSessionId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	tr, err := ytmodels.GetTempRadioBySecret(db, session_id)
	if err != nil {
		http.Error(w, "radio did not found", http.StatusNotFound)
		return
	}

	fb := new(FileBody)
	fb.Name = r.FormValue("Name")

	err = fto.Add(db, tr, fb.Name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = SetFile(r, fto.Id())
	if err != nil {
		fto.Delete(db)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fb.FileId = fto.Id().Hex()
	out, err := json.Marshal(fb)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write(out)
}

func DeleteFile(w http.ResponseWriter, r *http.Request) {
	db := ytutil.GetDB(r, ytglobals.DBName())
	vars := mux.Vars(r)
	ftname := vars["type"]
	ftid := vars["id"]
	fto := ytmodels.NewFileType(ftname)
	if fto == nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	session_id, err := ytutil.GetSessionId(r)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	tr, err := ytmodels.GetTempRadioBySecret(db, session_id)
	if err != nil {
		http.Error(w, "radio did not found", http.StatusNotFound)
		return
	}

	if !bson.IsObjectIdHex(ftid) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	err = tr.FindFile(db, ftname, bson.ObjectIdHex(ftid), fto)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	err = fto.Delete(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusAccepted)
}

func ListFiles(w http.ResponseWriter, r *http.Request) {
	db := ytutil.GetDB(r, ytglobals.DBName())
	vars := mux.Vars(r)
	ftname := vars["type"]

	session_id, err := ytutil.GetSessionId(r)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	tr, err := ytmodels.GetTempRadioBySecret(db, session_id)
	if err != nil {
		http.Error(w, "radio did not found", http.StatusNotFound)
		return
	}

	ls, err := tr.ListFiles(db, ftname)
	if err != nil {
		http.Error(w, "[]", http.StatusAccepted)
		return
	}

	out, err := json.Marshal(ls)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, "[]", http.StatusAccepted)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write(out)
}

var mimetypes = map[string][]string{
	"image":  []string{"image/jpeg", "image/gif", "image/png"},
	"sound":  []string{"audio/mpeg", "audio/x-wav", "application/ogg"},
	"record": []string{"audio/x-wav"},
}

func isCorrectMimeType(ftype string, buf []byte) bool {
	mm, err := magicmime.New(magicmime.MAGIC_MIME_TYPE | magicmime.MAGIC_SYMLINK | magicmime.MAGIC_ERROR)
	if err != nil {
		panic(err)
	}
	mt, err := mm.TypeByBuffer(buf)
	if err != nil {
		log.Println(err)
		return false
	}
	for _, v := range mimetypes[ftype] {
		if v == mt {
			return true
		}
	}
	return false
}

func SetFile(r *http.Request, id bson.ObjectId) error {
	vars := mux.Vars(r)
	ftname := vars["type"]
	buf_ls, err := ytutil.Files(r)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	for _, buf := range buf_ls {
		if !isCorrectMimeType(ftname, buf) {
			return errors.New("filetype is not the correct one")
		}
		path := fmt.Sprintf("static/files/%s/%s", ftname, id.Hex())
		err = ioutil.WriteFile(path, buf, os.ModePerm)
		if err != nil {
			log.Println(err.Error())
			return err
		}
	}
	return nil
}
