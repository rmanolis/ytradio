package ytcfiles

import (
	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {
	router.HandleFunc("/cookie", AddCookie).Methods("POST")

	router.HandleFunc("/files/{type}", AddFile).Methods("POST")
	router.HandleFunc("/files/{type}/{id}", DeleteFile).Methods("DELETE")
	router.HandleFunc("/files/{type}", ListFiles).Methods("GET")
}
