package ytmodels

import (
	"ytradio/server/ytmodels/ytmodelnames"
	"ytradio/server/ytutil"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Record struct {
	ID       bson.ObjectId `json:"Id" bson:"_id,omitempty"`
	Sname    string        `json:"Name" bson:"name"` //name is the session
	TradioId bson.ObjectId `json:"TempRadioId" bson:"tradioid"`
}

func (r *Record) Add(db *mgo.Database, tr *TempRadio, name string) error {
	c := db.C(ytmodelnames.Record)
	r.ID = bson.NewObjectId()
	r.TradioId = tr.Id
	r.Sname = name
	return c.Insert(r)
}

func (r *Record) Delete(db *mgo.Database) error {
	ytutil.DeleteFile(ytmodelnames.Record, r.ID.Hex())
	c := db.C(ytmodelnames.Record)
	return c.RemoveId(r.ID)
}

func (r *Record) Type() string {
	return "record"
}

func (r *Record) Name() string {
	return r.Sname
}

func (r *Record) TempRadioId() bson.ObjectId {
	return r.TradioId
}

func (r *Record) Id() bson.ObjectId {
	return r.ID
}

func GetRecordList(db *mgo.Database, trid bson.ObjectId) ([]Record, error) {
	c := db.C(ytmodelnames.Record)
	ls := []Record{}
	err := c.Find(bson.M{"tradioid": trid}).All(&ls)
	return ls, err
}
