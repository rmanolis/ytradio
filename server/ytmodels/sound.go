package ytmodels

import (
	"ytradio/server/ytmodels/ytmodelnames"
	"ytradio/server/ytutil"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Sound struct {
	ID       bson.ObjectId `json:"Id" bson:"_id,omitempty"`
	Sname    string        `json:"Name" bson:"name"` //name is the session
	TradioId bson.ObjectId `json:"TempRadioId" bson:"tradioid"`
}

func (r *Sound) Add(db *mgo.Database, tr *TempRadio, name string) error {
	c := db.C(ytmodelnames.Sound)
	r.ID = bson.NewObjectId()
	r.TradioId = tr.Id
	r.Sname = name
	return c.Insert(r)
}

func (r *Sound) Delete(db *mgo.Database) error {

	ytutil.DeleteFile(ytmodelnames.Sound, r.ID.Hex())
	c := db.C(ytmodelnames.Sound)
	return c.RemoveId(r.ID)
}

func (r *Sound) Type() string {
	return "sound"
}

func (r *Sound) Name() string {
	return r.Sname
}

func (r *Sound) TempRadioId() bson.ObjectId {
	return r.TradioId
}

func (r *Sound) Id() bson.ObjectId {
	return r.ID
}

func GetSoundList(db *mgo.Database, trid bson.ObjectId) ([]Sound, error) {
	c := db.C(ytmodelnames.Sound)
	ls := []Sound{}
	err := c.Find(bson.M{"tradioid": trid}).All(&ls)
	return ls, err
}
