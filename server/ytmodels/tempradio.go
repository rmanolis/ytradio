package ytmodels

import (
	"errors"
	"log"
	"ytradio/server/ytmodels/ytmodelnames"
	"ytradio/server/ytutil"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type TempRadio struct {
	Id        bson.ObjectId `bson:"_id,omitempty"`
	Name      string        //name is the session
	Listeners []string      //sessions
	Secret    string
}

func (tr *TempRadio) AddListener(listener string) {
	ls := append(tr.Listeners, listener)
	tr.Listeners = ls
}

func (tr *TempRadio) RemoveListener(listener string) {
	for i, v := range tr.Listeners {
		if v == listener {
			tr.Listeners = append(tr.Listeners[:i], tr.Listeners[i+1:]...)
		}
	}
}

func (tr *TempRadio) IsListener(lis string) bool {
	var is = false
	for _, v := range tr.Listeners {
		if v == lis {
			is = true
			break
		}
	}
	return is
}

func (tr *TempRadio) FindFile(db *mgo.Database,
	ftype string, file_id bson.ObjectId, ft FileType) error {
	c := db.C(ftype)
	return c.FindId(file_id).One(ft)
}

func (tr *TempRadio) ListFiles(db *mgo.Database, ftype string) (interface{}, error) {
	switch ftype {
	case "record":
		return GetRecordList(db, tr.Id)
	case "image":
		return GetImageList(db, tr.Id)
	case "sound":
		return GetSoundList(db, tr.Id)
	}
	return nil, errors.New("wrong type")
}

func GetTempRadio(db *mgo.Database, session string) (*TempRadio, error) {
	c := db.C(ytmodelnames.TempRadio)
	tr := new(TempRadio)
	err := c.Find(bson.M{"name": session}).One(&tr)
	return tr, err
}

func GetTempRadioBySecret(db *mgo.Database, secret string) (*TempRadio, error) {
	c := db.C(ytmodelnames.TempRadio)
	tr := new(TempRadio)
	err := c.Find(bson.M{"secret": secret}).One(&tr)
	return tr, err
}

func NewTempRadio(db *mgo.Database, name string) (*TempRadio, error) {
	c := db.C(ytmodelnames.TempRadio)
	tr := new(TempRadio)
	tr.Id = bson.NewObjectId()
	tr.Name = name
	tr.Secret = ytutil.RandSeq(11) + name
	return tr, c.Insert(tr)
}

func FindRadioByListener(db *mgo.Database,
	session string) (*TempRadio, error) {
	c := db.C(ytmodelnames.TempRadio)
	tr := new(TempRadio)
	err := c.Find(bson.M{"listeners": bson.M{"$elemMatch": bson.M{"$eq": session}}}).One(&tr)
	return tr, err
}

func FindRadio(db *mgo.Database, session string) (*TempRadio, error) {
	c := db.C(ytmodelnames.TempRadio)
	tr := new(TempRadio)
	err := c.Find(bson.M{"$or": []bson.M{bson.M{"name": session},
		bson.M{"listeners": bson.M{"$elemMatch": bson.M{"$eq": session}}}}}).One(&tr)
	return tr, err
}

func (tr *TempRadio) Update(db *mgo.Database) error {
	c := db.C(ytmodelnames.TempRadio)
	return c.UpdateId(tr.Id, tr)
}

func (tr *TempRadio) Delete(db *mgo.Database) error {
	rls, _ := GetRecordList(db, tr.Id)
	for _, v := range rls {
		v.Delete(db)
	}
	ils, err := GetImageList(db, tr.Id)
	if err != nil {
		log.Println(err.Error())
	}
	for _, v := range ils {
		err := v.Delete(db)
		if err != nil {
			log.Println(err.Error())
		}
	}
	sls, _ := GetSoundList(db, tr.Id)
	for _, v := range sls {
		v.Delete(db)
	}
	c := db.C(ytmodelnames.TempRadio)
	return c.RemoveId(tr.Id)
}
