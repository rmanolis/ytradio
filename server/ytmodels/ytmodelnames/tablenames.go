package ytmodelnames

const (
	TempRadio = "tempradio"
	Record    = "record"
	Image     = "image"
	Sound     = "sound"
	ChatUser  = "chatuser"
	Message   = "message"
)
