package ytmodels

import (
	"ytradio/server/ytmodels/ytmodelnames"
	"ytradio/server/ytutil"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Image struct {
	ID       bson.ObjectId `json:"Id" bson:"_id,omitempty"`
	Sname    string        `json:"Name" bson:"name"` //name is the session
	TradioId bson.ObjectId `json:"TempRadioId" bson:"tradioid"`
}

func (r *Image) Add(db *mgo.Database, tr *TempRadio, name string) error {
	c := db.C(ytmodelnames.Image)
	r.ID = bson.NewObjectId()
	r.TradioId = tr.Id
	r.Sname = name
	return c.Insert(r)
}

func (r *Image) Delete(db *mgo.Database) error {
	ytutil.DeleteFile(ytmodelnames.Image, r.ID.Hex())
	c := db.C(ytmodelnames.Image)
	return c.RemoveId(r.ID)
}

func (r *Image) Type() string {
	return "image"
}

func (r *Image) Name() string {
	return r.Sname
}

func (r *Image) TempRadioId() bson.ObjectId {
	return r.TradioId
}

func (r *Image) Id() bson.ObjectId {
	return r.ID
}

func GetImageList(db *mgo.Database, trid bson.ObjectId) ([]Image, error) {
	c := db.C(ytmodelnames.Image)
	ls := []Image{}
	err := c.Find(bson.M{"tradioid": trid}).All(&ls)
	return ls, err
}
