package ytmodels

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type FileType interface {
	Add(*mgo.Database, *TempRadio, string) error
	Delete(*mgo.Database) error
	Type() string
	Name() string
	TempRadioId() bson.ObjectId
	Id() bson.ObjectId
}

func NewFileType(ftname string) FileType {
	switch ftname {
	case "record":
		return new(Record)
	case "image":
		return new(Image)
	case "sound":
		return new(Sound)
	}
	return nil
}
