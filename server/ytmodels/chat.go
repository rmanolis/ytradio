package ytmodels

import (
	"time"
	"ytradio/server/ytmodels/ytmodelnames"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type ChatUser struct {
	Id          bson.ObjectId ` bson:"_id,omitempty"`
	Name        string
	Session     string
	TempRadioId bson.ObjectId
}

type Message struct {
	Id         bson.ObjectId ` bson:"_id,omitempty"`
	ChatUserId bson.ObjectId
	Name       string
	Text       string
	Date       time.Time
}

func NewChatUser(db *mgo.Database,
	trId bson.ObjectId, session string, name string) (*ChatUser, error) {
	c := db.C(ytmodelnames.ChatUser)
	chu := new(ChatUser)
	chu.Id = bson.NewObjectId()
	chu.Session = session
	chu.TempRadioId = trId
	chu.Name = name

	return chu, c.Insert(chu)
}

func FindChatUser(db *mgo.Database, trId bson.ObjectId, session string) (*ChatUser, error) {
	c := db.C(ytmodelnames.ChatUser)
	chu := new(ChatUser)
	err := c.Find(bson.M{"tempradioid": trId, "session": session}).One(&chu)
	return chu, err
}

func (cu *ChatUser) NewMessage(db *mgo.Database, text string) (*Message, error) {
	c := db.C(ytmodelnames.Message)
	msg := new(Message)
	msg.Id = bson.NewObjectId()
	msg.ChatUserId = cu.Id
	msg.Name = cu.Name
	msg.Date = time.Now()
	msg.Text = text
	return msg, c.Insert(msg)
}

func (cu *ChatUser) Update(db *mgo.Database) error {
	c := db.C(ytmodelnames.ChatUser)
	return c.UpdateId(cu.Id, cu)
}

func (tr *TempRadio) ChatUsers(db *mgo.Database) ([]ChatUser, error) {
	c := db.C(ytmodelnames.ChatUser)
	chuls := []ChatUser{}
	err := c.Find(bson.M{"tempradioid": tr.Id}).All(&chuls)
	return chuls, err
}
